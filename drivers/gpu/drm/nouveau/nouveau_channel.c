/*
 * Copyright 2005-2006 Stephane Marchesin
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * PRECISION INSIGHT AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "drmP.h"
#include "drm.h"
#include "nouveau_drv.h"
#include "nouveau_drm.h"
#include "nouveau_dma.h"

static inline struct nouveau_channel *
nouveau_channel_by_id(struct drm_device *dev, struct drm_file *file_priv, unsigned channel)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	if (unlikely(channel >= dev_priv->engine.fifo.channels))
		return 0;
	return dev_priv->fifos[channel];
}

struct nouveau_channel *
nouveau_channel_get(struct drm_device *dev, struct drm_file *file_priv, unsigned channel)
{
	struct nouveau_channel* chan;
	int is_capable = 0;
	long long cur, old;

	chan = nouveau_channel_by_id(dev, file_priv, channel);
	if(unlikely(!chan))
		return ERR_PTR(-ENOENT);

	/* guess that we have the appropriate magic and only the implicit user reference
	 * this will always be the case in normal usage
	 */
	old = file_priv->magic;
	cur = atomic64_cmpxchg(&chan->owner_refcnt, old, old + (1LL << 32));
	while (unlikely(cur != old)) {
		unsigned cur_magic = (unsigned)cur;
		if (unlikely(cur_magic != file_priv->magic)) {
			if(!cur_magic)
				return ERR_PTR(-ENOENT);
			else if(!is_capable) {
				if(!capable(CAP_SYS_ADMIN))
					return ERR_PTR(-EACCES);
				is_capable = 1;
			}
		}
		old = cur;
		cur = atomic64_cmpxchg(&chan->owner_refcnt, cur, cur + (1LL << 32));
	}
	return chan;
}

static int
nouveau_channel_pushbuf_ctxdma_init(struct nouveau_channel *chan)
{
	struct drm_device *dev = chan->dev;
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_bo *pb = chan->pushbuf_bo;
	struct nouveau_gpuobj *pushbuf = NULL;
	int ret;

	if (dev_priv->card_type >= NV_50) {
		ret = nouveau_gpuobj_dma_new(chan, NV_CLASS_DMA_IN_MEMORY, 0,
					     dev_priv->vm_end, NV_DMA_ACCESS_RO,
					     NV_DMA_TARGET_AGP, &pushbuf);
		chan->pushbuf_base = pb->bo.offset;
	} else
	if (pb->bo.mem.mem_type == TTM_PL_TT) {
		ret = nouveau_gpuobj_gart_dma_new(chan, 0,
						  dev_priv->gart_info.aper_size,
						  NV_DMA_ACCESS_RO, &pushbuf,
						  NULL);
		chan->pushbuf_base = pb->bo.mem.mm_node->start << PAGE_SHIFT;
	} else
	if (dev_priv->card_type != NV_04) {
		ret = nouveau_gpuobj_dma_new(chan, NV_CLASS_DMA_IN_MEMORY, 0,
					     dev_priv->fb_available_size,
					     NV_DMA_ACCESS_RO,
					     NV_DMA_TARGET_VIDMEM, &pushbuf);
		chan->pushbuf_base = pb->bo.mem.mm_node->start << PAGE_SHIFT;
	} else {
		/* NV04 cmdbuf hack, from original ddx.. not sure of it's
		 * exact reason for existing :)  PCI access to cmdbuf in
		 * VRAM.
		 */
		ret = nouveau_gpuobj_dma_new(chan, NV_CLASS_DMA_IN_MEMORY,
					     drm_get_resource_start(dev, 1),
					     dev_priv->fb_available_size,
					     NV_DMA_ACCESS_RO,
					     NV_DMA_TARGET_PCI, &pushbuf);
		chan->pushbuf_base = pb->bo.mem.mm_node->start << PAGE_SHIFT;
	}

	ret = nouveau_gpuobj_ref_add(dev, chan, 0, pushbuf, &chan->pushbuf);
	if (ret) {
		NV_ERROR(dev, "Error referencing pushbuf ctxdma: %d\n", ret);
		if (pushbuf != dev_priv->gart_info.sg_ctxdma)
			nouveau_gpuobj_del(dev, &pushbuf);
		return ret;
	}

	return 0;
}

static struct nouveau_bo *
nouveau_channel_user_pushbuf_alloc(struct drm_device *dev)
{
	struct nouveau_bo *pushbuf = NULL;
	int location, ret;

	if (nouveau_vram_pushbuf)
		location = TTM_PL_FLAG_VRAM;
	else
		location = TTM_PL_FLAG_TT;

	ret = nouveau_bo_new(dev, NULL, 65536, 0, location, 0, 0x0000, false,
			     true, &pushbuf);
	if (ret) {
		NV_ERROR(dev, "error allocating DMA push buffer: %d\n", ret);
		return NULL;
	}

	ret = nouveau_bo_pin(pushbuf, location);
	if (ret) {
		NV_ERROR(dev, "error pinning DMA push buffer: %d\n", ret);
		nouveau_bo_ref(NULL, &pushbuf);
		return NULL;
	}

	return pushbuf;
}

/* allocates and initializes a fifo for user space consumption */
int
nouveau_channel_alloc(struct drm_device *dev, struct nouveau_channel **chan_ret,
		      struct drm_file *file_priv,
		      uint32_t vram_handle, uint32_t tt_handle, uint32_t* notifier_handle)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_pgraph_engine *pgraph = &dev_priv->engine.graph;
	struct nouveau_fifo_engine *pfifo = &dev_priv->engine.fifo;
	struct nouveau_channel *chan;
	int channel, user;
	uint32_t fence_offset;
	int ret;

	/*
	 * Alright, here is the full story
	 * Nvidia cards have multiple hw fifo contexts (praise them for that,
	 * no complicated crash-prone context switches)
	 * We allocate a new context for each app and let it write to it
	 * directly (woo, full userspace command submission !)
	 * When there are no more contexts, you lost
	 */

	/* no more fifos. you lost. */
	if(!atomic_add_unless(&dev_priv->fifo_alloc_count, 1, pfifo->channels))
		return -ENOSPC;

	do {
		channel = find_first_zero_bit(dev_priv->fifo_alloc_map, pfifo->channels);
	} while(test_and_set_bit(channel, dev_priv->fifo_alloc_map));

	chan = dev_priv->fifos[channel];
	if(!chan)
	{
		chan = kzalloc(sizeof(struct nouveau_channel), GFP_KERNEL);
		chan->dev = dev;
		chan->id = channel;
		atomic64_set(&chan->owner_refcnt, -1LL << 32);
		smp_wmb();
		dev_priv->fifos[channel] = chan;
	}

	INIT_LIST_HEAD(&chan->nvsw.vbl_wait);
	INIT_LIST_HEAD(&chan->fence.pending);
	chan->vram_handle = vram_handle;
	chan->gart_handle = tt_handle;
	chan->file_priv = file_priv;

	/* never fails */
	WARN_ON(nouveau_fence_init(chan));

	NV_INFO(dev, "Allocating FIFO number %d\n", channel);

	/* Allocate DMA push buffer */
	chan->pushbuf_bo = nouveau_channel_user_pushbuf_alloc(dev);
	if (!chan->pushbuf_bo) {
		ret = -ENOMEM;
		NV_ERROR(dev, "pushbuf %d\n", ret);
		goto out;
	}

	nouveau_dma_pre_init(chan);

	/* Locate channel's user control regs */
	if (dev_priv->card_type < NV_40)
		user = NV03_USER(channel);
	else
	if (dev_priv->card_type < NV_50)
		user = NV40_USER(channel);
	else
		user = NV50_USER(channel);

	chan->user = ioremap(pci_resource_start(dev->pdev, 0) + user,
								PAGE_SIZE);
	if (!chan->user) {
		ret = -ENOMEM;
		NV_ERROR(dev, "ioremap of regs failed.\n");
		goto out;
	}
	chan->user_put = 0x40;
	chan->user_get = 0x44;

	/* Allocate space for per-channel fixed notifier memory */
	ret = nouveau_notifier_init_channel(chan);
	if (ret) {
		NV_ERROR(dev, "ntfy %d\n", ret);
		goto out;
	}

	/* Setup channel's default objects */
	ret = nouveau_gpuobj_channel_init(chan, vram_handle, tt_handle);
	if (ret) {
		NV_ERROR(dev, "gpuobj %d\n", ret);
		goto out;
	}

	/* Create a dma object for the push buffer */
	ret = nouveau_channel_pushbuf_ctxdma_init(chan);
	if (ret) {
		NV_ERROR(dev, "pbctxdma %d\n", ret);
		goto out;
	}

	mutex_lock(&dev_priv->engine.mutex);

	/* we need the spinlock to make sure we don't enable/disable while
	   the IRQ handler is running on another CPU, which would cause it
	   to restore the wrong value */

        spin_lock_irq(&dev_priv->context_switch_lock);
        /* disable the fifo caches */
	pfifo->reassign(dev, false);
        spin_unlock_irq(&dev_priv->context_switch_lock);

	/* Create a graphics context for new channel */
	ret = pgraph->create_context(chan);
	if(ret)
                NV_ERROR(dev, "pgraph create_context failed.\n");

        /* Construct inital RAMFC for new channel */
	if (!ret) {
		ret = pfifo->create_context(chan);
		if(ret)
	                NV_ERROR(dev, "pfifo create_context failed.\n");
	}

        spin_lock_irq(&dev_priv->context_switch_lock);
	pfifo->reassign(dev, true);
        spin_unlock_irq(&dev_priv->context_switch_lock);

	mutex_unlock(&dev_priv->engine.mutex);

	if(ret)
		goto out;

	/* this must be allocated at offset 0 because otherwise the GPU
	 * cries about it, so do this before allocating the M2MF notifier */
	ret = nouveau_notifier_alloc(chan, NvFence, 32, &fence_offset);
	if (fence_offset)
		ret = -ENOMEM;
	if(ret)
		goto out;

	ret = nouveau_dma_init(chan);
	if (ret)
		goto out;

	/* Named memory object area */
	if(notifier_handle) {
		ret = drm_gem_handle_create(file_priv, chan->notifier_bo->gem,
				notifier_handle);
		if(ret)
			goto out;
	}

	nouveau_debugfs_channel_init(chan);

	NV_INFO(dev, "%s: initialised FIFO %d\n", __func__, channel);

	{
		smp_wmb();
		if(file_priv)
			atomic64_set(&chan->owner_refcnt, ((2LL - 1) << 32) | file_priv->magic);
		else
			atomic64_set(&chan->owner_refcnt, ((1LL - 1) << 32));
		smp_wmb();
	}

	*chan_ret = chan;
	return 0;

out:
	nouveau_channel_destroy(chan);
	return ret;
}


/* stops a fifo */
void
nouveau_channel_destroy(struct nouveau_channel *chan)
{
	struct drm_device *dev = chan->dev;
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_pgraph_engine *pgraph = &dev_priv->engine.graph;
	struct nouveau_fifo_engine *pfifo = &dev_priv->engine.fifo;
	unsigned long flags;
	int ret;
	int channel = chan->id;

	NV_INFO(dev, "%s: freeing fifo %d\n", __func__, channel);

	nouveau_debugfs_channel_fini(chan);

	/* Give outstanding push buffers a chance to complete */
	spin_lock_irqsave(&chan->fence.lock, flags);
	nouveau_fence_update(chan);
	spin_unlock_irqrestore(&chan->fence.lock, flags);
	if (chan->fence.sequence != chan->fence.sequence_ack) {
		struct nouveau_fence *fence = NULL;

		ret = nouveau_fence_new(chan, &fence, true);
		if (ret == 0) {
			ret = nouveau_fence_wait(fence, NULL, false, false);
			nouveau_fence_unref((void *)&fence);
		}

		if (ret)
			NV_ERROR(dev, "Failed to idle channel %d.\n", channel);
	}

	/* Ensure all outstanding fences are signaled.  They should be if the
	 * above attempts at idling were OK, but if we failed this'll tell TTM
	 * we're done with the buffers.
	 */
	nouveau_fence_fini(chan);

	mutex_lock(&dev_priv->engine.mutex);
	/* This will prevent pfifo from switching channels. */
	spin_lock_irq(&dev_priv->context_switch_lock);
	pfifo->reassign(dev, false);
	spin_unlock_irq(&dev_priv->context_switch_lock);

	/* We want to give pgraph a chance to idle and get rid of all potential
	 * errors. We need to do this before the lock, otherwise the irq handler
	 * is unable to process them.
	 */
	if (pgraph->channel(dev) == chan)
		nouveau_wait_for_idle(dev);

	pgraph->fifo_access(dev, false);
	if (pgraph->channel(dev) == chan)
		pgraph->unload_context(dev);
	pgraph->destroy_context(chan);
	pgraph->fifo_access(dev, true);

	if (pfifo->channel_id(dev) == channel) {
		pfifo->disable(dev);
		pfifo->unload_context(dev);
		pfifo->enable(dev);
	}

	pfifo->destroy_context(chan);
	spin_lock_irq(&dev_priv->context_switch_lock);
	pfifo->reassign(dev, true);
	spin_unlock_irq(&dev_priv->context_switch_lock);
	mutex_unlock(&dev_priv->engine.mutex);

	/* Release the channel's resources */
	nouveau_gpuobj_ref_del(dev, &chan->pushbuf);
	if (chan->pushbuf_bo) {
		nouveau_bo_unpin(chan->pushbuf_bo);
		nouveau_bo_ref(NULL, &chan->pushbuf_bo);
	}
	nouveau_gpuobj_channel_takedown(chan);
	nouveau_notifier_takedown_channel(chan);
	if (chan->user)
		iounmap(chan->user);

	memset(&chan->file_priv, 0, (char*)(chan + 1) - (char*)&chan->file_priv);

	smp_mb();
	clear_bit(channel, dev_priv->fifo_alloc_map);
	smp_mb();
	atomic_dec(&dev_priv->fifo_alloc_count);
	smp_mb();
}

/* Releases the user and the kernel references to the channel
 *
 * returns -ENOENT if the channel was already closed and we only did the put
 */
static int
nouveau_channel_close(struct drm_device *dev, struct drm_file *file_priv, unsigned channel)
{
	struct nouveau_channel* chan;
	int is_capable = 0;
	long long cur, old;

	chan = nouveau_channel_by_id(dev, file_priv, channel);
	if(unlikely(!chan))
		return -ENOENT;

	/* guess that we have the appropriate magic and only the implicit user reference
	 * this will always be the case in normal usage
	 */
	old = file_priv->magic;
	cur = atomic64_cmpxchg(&chan->owner_refcnt, old, -1LL << 32);
	while (unlikely(cur != old)) {
		long long new;
		unsigned cur_magic = (unsigned)cur;
		if (unlikely(cur_magic != file_priv->magic)) {
			if(!cur_magic)
				return -ENOENT;
			else if(!is_capable) {
				/* TODO: use CAP_KILL here? */
				if(!capable(CAP_SYS_ADMIN))
					return -EACCES;
				is_capable = 1;
			}
		}
		new = (long long)((unsigned)(cur >> 32) - 1) << 32;
		old = cur;
		cur = atomic64_cmpxchg(&chan->owner_refcnt, cur, new) ;
	}

	if(cur < 0)
		nouveau_channel_destroy(chan);
	return 0;
}

/* cleans up all the fifos from file_priv */
void
nouveau_channel_cleanup(struct drm_device *dev, struct drm_file *file_priv)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_engine *engine = &dev_priv->engine;
	int i;

	NV_DEBUG(dev, "clearing FIFO enables from file_priv\n");
	for (i = 0; i < engine->fifo.channels; i++)
		nouveau_channel_close(dev, file_priv, i);
}

/***********************************
 * ioctls wrapping the functions
 ***********************************/

static int
nouveau_ioctl_fifo_alloc(struct drm_device *dev, void *data,
			 struct drm_file *file_priv)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct drm_nouveau_channel_alloc *init = data;
	struct nouveau_channel *chan;
	int ret;

	NOUVEAU_CHECK_INITIALISED_WITH_RETURN;

	if (dev_priv->engine.graph.accel_blocked)
		return -ENODEV;

	if (init->fb_ctxdma_handle == ~0 || init->tt_ctxdma_handle == ~0)
		return -EINVAL;

	ret = nouveau_channel_alloc(dev, &chan, file_priv,
				    init->fb_ctxdma_handle,
				    init->tt_ctxdma_handle,
				    &init->notifier_handle);
	if (ret)
		return ret;
	init->channel  = chan->id;

	if (chan->dma.ib_max)
		init->pushbuf_domains = NOUVEAU_GEM_DOMAIN_VRAM |
					NOUVEAU_GEM_DOMAIN_GART;
	else if (chan->pushbuf_bo->bo.mem.mem_type == TTM_PL_VRAM)
		init->pushbuf_domains = NOUVEAU_GEM_DOMAIN_VRAM;
	else
		init->pushbuf_domains = NOUVEAU_GEM_DOMAIN_GART;

	init->subchan[0].handle = NvM2MF;
	if (dev_priv->card_type < NV_50)
		init->subchan[0].grclass = 0x0039;
	else
		init->subchan[0].grclass = 0x5039;
	init->subchan[1].handle = NvSw;
	init->subchan[1].grclass = NV_SW;
	init->nr_subchan = 2;
	return ret;
}

static int
nouveau_ioctl_fifo_free(struct drm_device *dev, void *data,
			struct drm_file *file_priv)
{
	struct drm_nouveau_channel_free *cfree = data;

	NOUVEAU_CHECK_INITIALISED_WITH_RETURN;

	return nouveau_channel_close(dev, file_priv, cfree->channel);
}

/***********************************
 * finally, the ioctl table
 ***********************************/

struct drm_ioctl_desc nouveau_ioctls[] = {
	DRM_IOCTL_DEF(DRM_NOUVEAU_GETPARAM, nouveau_ioctl_getparam, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_SETPARAM, nouveau_ioctl_setparam, DRM_AUTH | DRM_MASTER | DRM_ROOT_ONLY | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_CHANNEL_ALLOC, nouveau_ioctl_fifo_alloc, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_CHANNEL_FREE, nouveau_ioctl_fifo_free, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GROBJ_ALLOC, nouveau_ioctl_grobj_alloc, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_NOTIFIEROBJ_ALLOC, nouveau_ioctl_notifier_alloc, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GPUOBJ_FREE, nouveau_ioctl_gpuobj_free, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GEM_NEW, nouveau_gem_ioctl_new, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GEM_PUSHBUF, nouveau_gem_ioctl_pushbuf, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GEM_CPU_PREP, nouveau_gem_ioctl_cpu_prep, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GEM_CPU_FINI, nouveau_gem_ioctl_cpu_fini, DRM_AUTH | DRM_UNLOCKED),
	DRM_IOCTL_DEF(DRM_NOUVEAU_GEM_INFO, nouveau_gem_ioctl_info, DRM_AUTH | DRM_UNLOCKED),
};

int nouveau_max_ioctl = DRM_ARRAY_SIZE(nouveau_ioctls);
