/*
 * Copyright (C) 2007 Ben Skeggs.
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "drmP.h"
#include "drm.h"

#include "nouveau_drv.h"
#include "nouveau_dma.h"

#define USE_REFCNT (dev_priv->card_type >= NV_10)

#define NOUVEAU_SEM_BO_SIZE PAGE_SIZE

/* reading fences can be very expensive
 * use a threshold that would only use up half a single sem_bo
 */
#define NOUVEAU_SEM_MIN_THRESHOLD (NOUVEAU_SEM_BO_SIZE / (NOUVEAU_MAX_CHANNEL_NR * 2))

struct nouveau_fence {
	struct nouveau_channel *channel;
	struct kref refcount;
	struct list_head entry;

	uint32_t sequence;
	bool signalled;

	struct nouveau_sem_bo *sem_bo;
	int sem_num;
};

static inline struct nouveau_fence *
nouveau_fence(void *sync_obj)
{
	return (struct nouveau_fence *)sync_obj;
}

struct nouveau_sem_bo {
	struct nouveau_sem_bo *next;
	struct nouveau_bo *bo;
	uint32_t handle;

	/* >= 0: num_free + 1 slots are free, sem_bo is or is about to be on free_list
	    -1: all allocated, sem_bo is NOT on free_list
	*/
	atomic_t num_free;

	DECLARE_BITMAP(free_slots, NOUVEAU_SEM_BO_SIZE / sizeof(uint32_t));
	DECLARE_BITMAP(values, NOUVEAU_SEM_BO_SIZE / sizeof(uint32_t));
	DECLARE_BITMAP(channels, NOUVEAU_MAX_CHANNEL_NR);
};

struct nouveau_sem {
	struct nouveau_sem_bo *sem_bo;
	unsigned num;
	uint32_t value;
};

static struct nouveau_sem_bo*
nouveau_sem_bo_alloc(struct drm_device *dev)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_sem_bo *sem_bo;
	struct nouveau_bo *bo;
	int flags = TTM_PL_FLAG_VRAM;
	int ret;
	bool is_iomem;
	void *mem;
	unsigned handle;

	do {
		handle = dev_priv->sem.handles;
		if (handle >= dev_priv->sem.max_handles)
			return NULL;
	} while (cmpxchg(&dev_priv->sem.handles, handle, handle + 1) != handle);

	sem_bo = kmalloc(sizeof(*sem_bo), GFP_KERNEL);
	if (!sem_bo)
		return NULL;

	sem_bo->handle = NvSem + handle;

	ret = nouveau_bo_new(dev, NULL, NOUVEAU_SEM_BO_SIZE, 0, flags,
			0, 0x0000, true, true, &bo);
	if (ret)
		goto out_free;

	sem_bo->bo = bo;

	ret = nouveau_bo_pin(bo, flags);
	if (ret)
		goto out_bo;

	ret = nouveau_bo_map(bo);
	if (ret)
		goto out_unpin;

	mem = ttm_kmap_obj_virtual(&bo->kmap, &is_iomem);
	if (is_iomem)
		memset_io((void __force __iomem *)mem, 0, NOUVEAU_SEM_BO_SIZE);
	else
		memset(mem, 0, NOUVEAU_SEM_BO_SIZE);

	nouveau_bo_unmap(bo);

	memset((void *)sem_bo->free_slots, 0xff, sizeof(sem_bo->free_slots));
	memset((void *)sem_bo->values, 0xff, sizeof(sem_bo->values));
	atomic_set(&sem_bo->num_free, sizeof(sem_bo->free_slots) * 8 - 1);

	memset((void *)sem_bo->channels, 0, sizeof(sem_bo->channels));

	return sem_bo;

out_unpin:
	nouveau_bo_unpin(sem_bo->bo);
out_bo:
	nouveau_bo_ref(NULL, &sem_bo->bo);
out_free:
	kfree(sem_bo);
	return NULL;
}

static void
nouveau_sem_bo_channel_dtor(struct drm_device *dev,
			     struct nouveau_gpuobj *gpuobj) {
	struct nouveau_sem_bo *sem_bo;
	struct nouveau_channel *chan;

	if (!gpuobj->priv)
		return;

	chan = gpuobj->im_channel;
	sem_bo = gpuobj->priv;

	clear_bit(chan->id, sem_bo->channels);
	smp_wmb();
}

static int
nouveau_sem_bo_channel_init(struct nouveau_sem_bo *sem_bo, struct nouveau_channel *chan)
{
	struct drm_device *dev = chan->dev;
	struct nouveau_gpuobj *obj = NULL;
	int ret;

	if (test_bit(chan->id, sem_bo->channels))
		return 0;

	if (WARN_ON(sem_bo->bo->bo.mem.mem_type != TTM_PL_VRAM))
		return -EINVAL;

	ret = nouveau_gpuobj_dma_new(chan, NV_CLASS_DMA_IN_MEMORY,
		sem_bo->bo->bo.mem.mm_node->start, NOUVEAU_SEM_BO_SIZE,
		NV_DMA_ACCESS_RW, NV_DMA_TARGET_VIDMEM, &obj);
	if (ret)
		return ret;

	obj->dtor = nouveau_sem_bo_channel_dtor;
	obj->priv = sem_bo;

	ret = nouveau_gpuobj_ref_add(dev, chan, sem_bo->handle, obj, NULL);
	if (ret) {
		nouveau_gpuobj_del(dev, &obj);
		return ret;
	}

	set_bit(chan->id, sem_bo->channels);
	smp_wmb();

	return 0;
}

static void
nouveau_sem_bo_free(struct nouveau_sem_bo *sem_bo)
{
	nouveau_bo_unpin(sem_bo->bo);
	nouveau_bo_ref(NULL, &sem_bo->bo);
	kfree(sem_bo);
}

static inline void
nouveau_sem_bo_enqueue(struct drm_device *dev, struct nouveau_sem_bo *sem_bo)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	unsigned long flags;

	spin_lock_irqsave(&dev_priv->sem.free_list_lock, flags);
	sem_bo->next = dev_priv->sem.free_list;
	dev_priv->sem.free_list = sem_bo;
	spin_unlock_irqrestore(&dev_priv->sem.free_list_lock, flags);
}

static int
nouveau_sem_alloc(struct drm_device *dev, struct nouveau_sem *sem)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_sem_bo *sem_bo = NULL;
	int v;

retry:
	sem_bo = dev_priv->sem.free_list;
	if (!sem_bo) {
		sem_bo = nouveau_sem_bo_alloc(dev);
		if (!sem_bo)
			return -ENOMEM;

		atomic_dec(&sem_bo->num_free);
		nouveau_sem_bo_enqueue(dev, sem_bo);
	} else {
		int num_free;
retry_num_free:
		num_free = atomic_read(&sem_bo->num_free);
		if (unlikely(num_free <= 0)) {
			unsigned long flags;
			if (unlikely(num_free < 0))
				goto retry;

			spin_lock_irqsave(&dev_priv->sem.free_list_lock, flags);
			if (unlikely(sem_bo != dev_priv->sem.free_list)) {
				spin_unlock_irqrestore(&dev_priv->sem.free_list_lock, flags);
				goto retry;
			}

			dev_priv->sem.free_list = sem_bo->next;
			/* Someone may have incremented the count in the meantime.
			 * In this case, revert the above line and put it back on the free list.
			 *
			 * Note that we can't just decrement before removing from the list,
			 * since otherwise an increment could put sem_bo in the free_list twice,
			 * corrupting it.
			 *
			 * Note that num_free cannot already be -1 because we just checked that
			 * sem_bo is still the head of the free list, and we are holding free_list_lock.
			 *
			 * atomic_dec_return is a memory barrier, so this is fine.
			 */
			if (atomic_dec_return(&sem_bo->num_free) >= 0)
				dev_priv->sem.free_list = sem_bo;

			spin_unlock_irqrestore(&dev_priv->sem.free_list_lock, flags);
		} else if (unlikely(atomic_cmpxchg(&sem_bo->num_free, num_free, num_free - 1) != num_free))
			goto retry_num_free;
	}

retry_bit:
	v = find_first_bit(sem_bo->free_slots, sizeof(sem_bo->free_slots) * 8);

	/* we reserved our bit by decrementing num_free, so this doesn't happen
	   however, the first available bit may have been taken */
	if (WARN_ON(v >= sizeof(sem_bo->free_slots) * 8))
		goto retry;

	if (unlikely(!test_and_clear_bit(v, sem_bo->free_slots)))
		goto retry_bit;

	sem->sem_bo = sem_bo;
	sem->value = test_and_change_bit(v, sem_bo->values);
	sem->num = v;

	return 0;
}

static void
nouveau_sem_release(struct drm_device *dev, struct nouveau_sem_bo *sem_bo, int i)
{
	set_bit(i, sem_bo->free_slots);

	if (atomic_inc_and_test(&sem_bo->num_free))
		nouveau_sem_bo_enqueue(dev, sem_bo);
}

static void
nouveau_fence_del(struct kref *ref)
{
	struct nouveau_fence *fence =
		container_of(ref, struct nouveau_fence, refcount);

	kfree(fence);
}

static inline void
nouveau_sem_emit(struct nouveau_channel *chan, struct nouveau_sem *sem, unsigned op)
{
	uint32_t handle = sem->sem_bo->handle;
	if (chan->sem.handle != handle) {
		BEGIN_RING(chan, NvSubSw, NV_SW_DMA_SEMAPHORE, 1);
		OUT_RING(chan, handle);
		chan->sem.handle = handle;
	}
	if (chan->sem.num != sem->num) {
		BEGIN_RING(chan, NvSubSw, NV_SW_SEMAPHORE_OFFSET, 1);
		OUT_RING(chan, sem->num << 2);
		chan->sem.num = sem->num;
	}
	BEGIN_RING(chan, NvSubSw, op, 1);
	OUT_RING(chan, sem->value);
}

/* Currently this ignores waited_fence->sequence and syncs the last fence on waited_fence->channel
 * If a better GPU synchronization mechanism is discovered, then the actual fence may be used.
 * Note that sem_fence is a fence on the *waiting *channel, used to free the semaphore.
 */
struct nouveau_fence*
nouveau_fence_sync(struct nouveau_fence *waited_fence, struct nouveau_channel *chan)
{
	struct nouveau_channel *waited_chan = waited_fence->channel;
	struct drm_device *dev;
	struct drm_nouveau_private *dev_priv;
	struct nouveau_sem sem;
	uint32_t handle;
	int ret;
	struct nouveau_fence *sem_fence;
	unsigned long flags;

	dev = chan->dev;
	dev_priv = chan->dev->dev_private;

	if (dev_priv->chipset < 0x17)
		return ERR_PTR(-ENOSYS);

	/* try to reclaim semaphores when we hit the threshold
	   this helps keeping a low number of active semaphores

	   Note that in the DRI2 case this is never triggered
	   since we wait for fences on both channels.

	   However, if buffers were all different, this could be
	   necessary.
	*/
	if (atomic_read(&chan->fence.sem_count) >= chan->fence.sem_threshold) {
		spin_lock_irqsave(&chan->fence.lock, flags);
		if (atomic_read(&chan->fence.sem_count) >= chan->fence.sem_threshold)
			nouveau_fence_update(chan);
		spin_unlock_irqrestore(&chan->fence.lock, flags);
	}

	ret = nouveau_fence_new(chan, &sem_fence, 0);
	if (ret)
		return ERR_PTR(ret);

	ret = nouveau_sem_alloc(chan->dev, &sem);
	if (ret) {
		kfree(sem_fence);
		return ERR_PTR(ret);
	}

	BUG_ON(!sem.sem_bo);

	ret = nouveau_sem_bo_channel_init(sem.sem_bo, chan);
	if (!ret)
		ret = nouveau_sem_bo_channel_init(sem.sem_bo, waited_chan);
	if (ret) {
		nouveau_sem_release(dev, sem.sem_bo, sem.num);
		kfree(sem_fence);
		return ERR_PTR(ret);
	}

	handle = sem.sem_bo->handle;

/*	NV_DEBUG(dev, "sync %i <- %i with %x:%i (sem %i/%i)\n", chan->id, waited_chan->id, sem.sem_bo->handle, sem.num, atomic_read(&chan->fence.sem_count), chan->fence.sem_threshold); */

	sem_fence->sem_bo = sem.sem_bo;
	sem_fence->sem_num = sem.num;

	atomic_inc(&chan->fence.sem_count);

	mutex_lock(&waited_chan->dma.mutex);
	ret = RING_SPACE(waited_chan, 6);
	if (ret) {
		mutex_unlock(&waited_chan->dma.mutex);
		return ERR_PTR(ret);
	}

	nouveau_sem_emit(waited_chan, &sem, NV_SW_SEMAPHORE_RELEASE);
	FIRE_RING(waited_chan);
	mutex_unlock(&waited_chan->dma.mutex);

	mutex_lock(&chan->dma.mutex);
	ret = RING_SPACE(chan, 6 + 2);
	if (ret) {
		mutex_unlock(&chan->dma.mutex);
		return ERR_PTR(ret);
	}

	nouveau_sem_emit(chan, &sem, NV_SW_SEMAPHORE_ACQUIRE);

	nouveau_fence_emit(sem_fence);
	mutex_unlock(&chan->dma.mutex);

	return sem_fence;
}

static void
nouveau_fence_complete(struct nouveau_fence *fence)
{
	if (fence->sem_bo) {
		nouveau_sem_release(fence->channel->dev, fence->sem_bo, fence->sem_num);
		atomic_dec(&fence->channel->fence.sem_count);
	}

	fence->signalled = true;
	list_del(&fence->entry);
	kref_put(&fence->refcount, nouveau_fence_del);
}

void
nouveau_fence_update(struct nouveau_channel *chan)
{
	struct drm_nouveau_private *dev_priv = chan->dev->dev_private;
	struct list_head *entry, *tmp;
	struct nouveau_fence *fence;
	uint32_t sequence;
	unsigned sem_threshold;

	if (USE_REFCNT)
		sequence = nvchan_rd32(chan, 0x48);
	else
		sequence = chan->fence.last_sequence_irq;

	if (chan->fence.sequence_ack == sequence)
		return;
	chan->fence.sequence_ack = sequence;

	list_for_each_safe(entry, tmp, &chan->fence.pending) {
		fence = list_entry(entry, struct nouveau_fence, entry);

		sequence = fence->sequence;
		nouveau_fence_complete(fence);

		if (sequence == chan->fence.sequence_ack)
			break;
	}

	sem_threshold = atomic_read(&chan->fence.sem_count) * 2;
	if (sem_threshold < NOUVEAU_SEM_MIN_THRESHOLD)
		sem_threshold = NOUVEAU_SEM_MIN_THRESHOLD;
	chan->fence.sem_threshold = sem_threshold;
}

int
nouveau_fence_new(struct nouveau_channel *chan, struct nouveau_fence **pfence,
		  bool emit)
{
	struct nouveau_fence *fence;
	int ret = 0;

	fence = kzalloc(sizeof(*fence), GFP_KERNEL);
	if (!fence)
		return -ENOMEM;
	kref_init(&fence->refcount);
	fence->channel = chan;

	if (emit)
		ret = nouveau_fence_emit(fence);

	if (ret)
		nouveau_fence_unref((void *)&fence);
	*pfence = fence;
	return ret;
}

struct nouveau_channel *
nouveau_fence_channel(struct nouveau_fence *fence)
{
	return fence ? fence->channel : NULL;
}

int
nouveau_fence_emit(struct nouveau_fence *fence)
{
	struct drm_nouveau_private *dev_priv = fence->channel->dev->dev_private;
	struct nouveau_channel *chan = fence->channel;
	unsigned long flags;
	int ret;

	ret = RING_SPACE(chan, 2);
	if (ret)
		return ret;

	if (unlikely(chan->fence.sequence == chan->fence.sequence_ack - 1)) {
		spin_lock_irqsave(&chan->fence.lock, flags);
		nouveau_fence_update(chan);
		spin_unlock_irqrestore(&chan->fence.lock, flags);

		BUG_ON(chan->fence.sequence ==
		       chan->fence.sequence_ack - 1);
	}

	fence->sequence = ++chan->fence.sequence;

	kref_get(&fence->refcount);
	spin_lock_irqsave(&chan->fence.lock, flags);
	list_add_tail(&fence->entry, &chan->fence.pending);
	spin_unlock_irqrestore(&chan->fence.lock, flags);

	BEGIN_RING(chan, NvSubSw, USE_REFCNT ? 0x0050 : 0x0150, 1);
	OUT_RING(chan, fence->sequence);
	FIRE_RING(chan);

	return 0;
}

void
nouveau_fence_unref(void **sync_obj)
{
	struct nouveau_fence *fence = nouveau_fence(*sync_obj);

	if (fence)
		kref_put(&fence->refcount, nouveau_fence_del);
	*sync_obj = NULL;
}

void *
nouveau_fence_ref(void *sync_obj)
{
	struct nouveau_fence *fence = nouveau_fence(sync_obj);

	kref_get(&fence->refcount);
	return sync_obj;
}

bool
nouveau_fence_signalled(void *sync_obj, void *sync_arg)
{
	struct nouveau_fence *fence = nouveau_fence(sync_obj);
	struct nouveau_channel *chan = fence->channel;
	unsigned long flags;

	if (fence->signalled)
		return true;

	spin_lock_irqsave(&chan->fence.lock, flags);
	nouveau_fence_update(chan);
	spin_unlock_irqrestore(&chan->fence.lock, flags);
	return fence->signalled;
}

int
nouveau_fence_wait(void *sync_obj, void *sync_arg, bool lazy, bool intr)
{
	unsigned long timeout = jiffies + (3 * DRM_HZ);
	int ret = 0;

	__set_current_state(intr ? TASK_INTERRUPTIBLE : TASK_UNINTERRUPTIBLE);

	while (1) {
		if (nouveau_fence_signalled(sync_obj, sync_arg))
			break;

		if (time_after_eq(jiffies, timeout)) {
			ret = -EBUSY;
			break;
		}

		if (lazy)
			schedule_timeout(1);

		if (intr && signal_pending(current)) {
			ret = -ERESTARTSYS;
			break;
		}
	}

	__set_current_state(TASK_RUNNING);

	return ret;
}

int
nouveau_fence_flush(void *sync_obj, void *sync_arg)
{
	return 0;
}

void
nouveau_fence_handler(struct drm_device *dev, int channel)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_channel *chan = NULL;

	if (channel >= 0 && channel < dev_priv->engine.fifo.channels)
		chan = dev_priv->fifos[channel];

	if (chan) {
		spin_lock_irq(&chan->fence.lock);
		nouveau_fence_update(chan);
		spin_unlock_irq(&chan->fence.lock);
	}
}

int
nouveau_fence_init(struct nouveau_channel *chan)
{
	INIT_LIST_HEAD(&chan->fence.pending);
	spin_lock_init(&chan->fence.lock);
	atomic_set(&chan->fence.sem_count, 0);
	chan->fence.sem_threshold = NOUVEAU_SEM_MIN_THRESHOLD;
	chan->sem.handle = 0;
	chan->sem.num = ~0;
	return 0;
}

void
nouveau_fence_fini(struct nouveau_channel *chan)
{
	struct list_head *entry, *tmp;
	struct nouveau_fence *fence;

	list_for_each_safe(entry, tmp, &chan->fence.pending) {
		fence = list_entry(entry, struct nouveau_fence, entry);

		nouveau_fence_complete(fence);
	}
}

void
nouveau_fence_device_init(struct drm_device *dev)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	spin_lock_init(&dev_priv->sem.free_list_lock);
	dev_priv->sem.free_list = NULL;
	dev_priv->sem.handles = 0;
	/* these are each pinned and 4KB, providing 1024 semaphores each
	   we should need only one in normal circumstances */
	dev_priv->sem.max_handles = 16;
}

void
nouveau_fence_device_takedown(struct drm_device *dev)
{
	struct drm_nouveau_private *dev_priv = dev->dev_private;
	struct nouveau_sem_bo *sem_bo, *next;
	/* all the sem_bos allocated must be in the free list since all channels
	* and thus fences have already been terminated */
	for (sem_bo = dev_priv->sem.free_list; sem_bo; sem_bo = next) {
		next = sem_bo->next;
		nouveau_sem_bo_free(sem_bo);
	}
}
